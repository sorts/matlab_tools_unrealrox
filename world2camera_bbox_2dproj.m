function [bbox_2d_points]=world2camera_bbox_2dproj( orig_3d_bbox, camera_pos, camera_rot, obj_pos, obj_rot, camera_K )
%WORLD2CAMERA_BBOX_2DPROJ projects to the camera plane 3D bbox corners
%   3D bbox in world coordiantes is converted to camera coordinates and
%   projected to the image plane (2D coordinate)
   
    obj_rot = degtorad(obj_rot);
    obj_rot = eul2rot(obj_rot);

    orig_3d_bbox = [ [0;0;0;], orig_3d_bbox];
    % roto-translate bounding box, according to object position and orientation
    oriented_3d_bbox = obj_rot * orig_3d_bbox +repmat(obj_pos, [1 size(orig_3d_bbox, 2)]);

    % world2camera
    camera_rot = degtorad(camera_rot);
    camera_rot = eul2rot(camera_rot);
    
    Rt=-(transpose(camera_rot)*camera_pos);
    RotoTranslation = [transpose(camera_rot),Rt];
    RotoTranslation = [RotoTranslation; 0 0 0 1];
    
    % list 3d->2d projected points (image plane)
    bbox_2d_points = [];
    for p = 1:size(orig_3d_bbox, 2)   
        bbox_point_3d = oriented_3d_bbox(:,p);
        bbox_point_3d = [bbox_point_3d; 1];

        bbox_point_3d = RotoTranslation * bbox_point_3d;
        bbox_point_3d(1) = bbox_point_3d(1) / bbox_point_3d(3);
        bbox_point_3d(2) = bbox_point_3d(2) / bbox_point_3d(3);
        bbox_point_3d(3) = 1;

        K = [camera_K.fx 0 camera_K.cx; 0 camera_K.fy camera_K.cy; 0 0 1];
        bbox_point_2d = K*bbox_point_3d(1:3);
        bbox_point_2d(1:2) = ceil(bbox_point_2d(1:2));

        % append 2d points
        bbox_2d_points = [bbox_2d_points, bbox_point_2d ]; 
    end
end

