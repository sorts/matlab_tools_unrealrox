function draw_3d_projected_bbox_2d(obj_bbox_2d)
   %
   plot( obj_bbox_2d(1,1), obj_bbox_2d(2,1), 'bo' );       
   plot( obj_bbox_2d(1,2), obj_bbox_2d(2,2), 'bo' ); 
   plot( obj_bbox_2d(1,3), obj_bbox_2d(2,3), 'bo' ); 
   plot( obj_bbox_2d(1,4), obj_bbox_2d(2,4), 'bo' ); 
   plot( obj_bbox_2d(1,5), obj_bbox_2d(2,5), 'bo' ); 
   plot( obj_bbox_2d(1,6), obj_bbox_2d(2,6), 'bo' ); 
   plot( obj_bbox_2d(1,7), obj_bbox_2d(2,7), 'bo' ); 
   plot( obj_bbox_2d(1,8), obj_bbox_2d(2,8), 'bo' ); 
   plot( obj_bbox_2d(1,9), obj_bbox_2d(2,9), 'bo' ); 
   
   plot(obj_bbox_2d(1,2:3),obj_bbox_2d(2,2:3),'red');
   plot(obj_bbox_2d(1,3:4),obj_bbox_2d(2,3:4),'red');
   plot(obj_bbox_2d(1,4:5),obj_bbox_2d(2,4:5),'red');
   plot(obj_bbox_2d(1,6:7),obj_bbox_2d(2,6:7),'red');
   plot(obj_bbox_2d(1,7:8),obj_bbox_2d(2,7:8),'red');
   plot(obj_bbox_2d(1,8:9),obj_bbox_2d(2,8:9),'red');
   plot( [ obj_bbox_2d(1,2), obj_bbox_2d(1,5) ] ,  [ obj_bbox_2d(2,2), obj_bbox_2d(2,5) ],'red');
   plot( [ obj_bbox_2d(1,3), obj_bbox_2d(1,7) ] ,  [ obj_bbox_2d(2,3), obj_bbox_2d(2,7) ],'red');
   plot( [ obj_bbox_2d(1,4), obj_bbox_2d(1,8) ] ,  [ obj_bbox_2d(2,4), obj_bbox_2d(2,8) ],'red');
   plot( [ obj_bbox_2d(1,2), obj_bbox_2d(1,6) ] ,  [ obj_bbox_2d(2,2), obj_bbox_2d(2,6) ],'red');
   plot( [ obj_bbox_2d(1,5), obj_bbox_2d(1,9) ] ,  [ obj_bbox_2d(2,5), obj_bbox_2d(2,9) ],'red');
   plot( [ obj_bbox_2d(1,6), obj_bbox_2d(1,9) ] ,  [ obj_bbox_2d(2,6), obj_bbox_2d(2,9) ],'red');
end