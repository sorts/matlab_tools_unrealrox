function [ points_2d ] = world2camera_3dpoint_2dproj( points_3d, camera_pos, camera_rot, camera_K )
%WORLD2CAMERA_3dpoints_2DPROJ projects to the camera plane 3D points
%   3D points in world coordiantes are converted to camera coordinates and
%   projected to the image plane (2D coordinate)
   
    % world2camera
    camera_rot = degtorad(camera_rot);
    camera_rot = eul2rot(camera_rot);
    
    Rt=-(transpose(camera_rot)*camera_pos);
    RotoTranslation = [transpose(camera_rot),Rt];
    RotoTranslation = [RotoTranslation; 0 0 0 1];
    
    % list 3d->2d projected points (image plane)
    points_2d = [];
    for p = 1:size(points_3d, 2)   
        point_3d = points_3d(:,p);
        point_3d = [point_3d; 1];

        point_3d = RotoTranslation * point_3d;
        point_3d(1) = point_3d(1) / point_3d(3);
        point_3d(2) = point_3d(2) / point_3d(3);
        point_3d(3) = 1;

        K = [camera_K.fx 0 camera_K.cx; 0 camera_K.fy camera_K.cy; 0 0 1];
        point_2d = K*point_3d(1:3);
        point_2d(1:2) = ceil(point_2d(1:2));

        % append 2d points
        points_2d = [points_2d, point_2d ]; 
    end
end
