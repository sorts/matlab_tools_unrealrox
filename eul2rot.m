function R = eul2rot(eul)

    R_x = [[1 0 0]; [0 cos(eul(1)) -sin(eul(1))]; [0 sin(eul(1)) cos(eul(1))]];
    R_y = [[cos(eul(2)) 0 sin(eul(2))]; [0 1 0]; [-sin(eul(2)), 0, cos(eul(2))]];
    R_z = [[cos(eul(3)) -sin(eul(3)) 0]; [sin(eul(3)) cos(eul(3)) 0]; [0 0 1];];

    R = R_y * R_x * R_z;
end