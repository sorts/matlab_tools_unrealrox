function [XYZ  hasDepth]= depth_to_pts(Cx,Cy,Fx,Fy, depth, width,height)
    depth = double(depth);
    [x,y] = meshgrid(1:width, 1:height);
    XYZ(:,:,1) = (x-Cx).*depth/Fx;
    XYZ(:,:,2) = (y-Cy).*depth/Fy;
    XYZ(:,:,3) = depth;
    XYZ(:,:,4) = depth~=0;
    
    points = reshape(XYZ, [], 4)';
    hasDepth = XYZ(:,:,4) > 1e-4;
    hasDepth=hasDepth(:);
    XYZ = points(1:3, hasDepth);   
end