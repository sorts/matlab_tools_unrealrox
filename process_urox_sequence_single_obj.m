%close all
%clear

out_path='D:\\testBBOX3D\\000\\processed_seq\\';
mkdir(out_path);
mkdir([out_path 'Debug']);

json_recording_path_filename = 'C:\\000_mod.json';
json_scene_filename = 'sceneObject.json';
sequence_path = 'D:\\testBBOX3D\\000\\';

dat=loadjson([json_recording_path_filename]);

% json recording header
total_num_frames = getfield(dat(1),'total_frames');
cameras = getfield(dat(1),'cameras');
objects = getfield(dat(1),'objects');
static_objects = getfield(dat(1),'non_movable_objects');
frames = getfield(dat(1),'frames');

% for each obj, save origin axis aligned 3d bbox, create dictionary
objects_bbox = containers.Map
for i = 1:numel(objects)
    object_3d_box = [];
    for c = 1:8
         object_3d_box = [object_3d_box, [objects{i}.obb{c}.y; -objects{i}.obb{c}.z; objects{i}.obb{c}.x;] ]; % UE -> CV coordinates
    end
    objects_bbox( objects{i}.name ) = object_3d_box*10; % convert to mm
    
    mkdir([out_path objects{i}.name]);
    mkdir([out_path objects{i}.name '\\JPEGImages']);
    mkdir([out_path objects{i}.name '\\labels']);
    mkdir([out_path objects{i}.name '\\mask']);
end

% load json object scenes
scene_objects_json = loadjson([sequence_path json_scene_filename]);
scene_objects = getfield(scene_objects_json(1),'SceneObjects');

% create dictionary for rgb masks
objects_rgb_mask = containers.Map
for i = 1:numel(scene_objects)
    rgb_mask =[uint8(scene_objects{i}.instance_color.r); uint8(scene_objects{i}.instance_color.g); uint8(scene_objects{i}.instance_color.b)];
    objects_rgb_mask( scene_objects{i}.instance_name ) = rgb_mask;
end

% load camera parameters
camera_K.height=1080;
camera_K.width=1920;
camera_K.fx = 960;
camera_K.fy = 960;
camera_K.cx = camera_K.width/2;
camera_K.cy = camera_K.height/2;

% for each frame
for f = 1:total_num_frames
    % for each camera
    for c = 1:numel(cameras)
       % load color img
       
       rgb_frame_path = sprintf('%s\\rgb\\%s\\%06d.jpg',sequence_path,cameras{c}.name,frames{f}.id_generated);
       depth_frame_path = sprintf('%s\\depth\\%s\\%06d.png',sequence_path,cameras{c}.name,frames{f}.id_generated);
       mask_frame_path = sprintf('%s\\mask\\%s\\%06d.png',sequence_path,cameras{c}.name,frames{f}.id_generated);
       
       color_img=imread(rgb_frame_path);
       depth_img=imread(depth_frame_path);
       mask_img=imread(mask_frame_path);
       
       % camera position and rotation
       camera=frames{f}.cameras{c};
       camera_pos = [camera.position.y; -camera.position.z; camera.position.x]*10; % UE to CV
       camera_rot = [camera.rotation.p; camera.rotation.y; camera.rotation.r];
          
       img_plot = imshow( color_img ) ;
       hold on ;
       
       % for each object check if it's in the camera viewpoint
       for o = 1:numel( frames{f}.objects )
           %disp(frames{f}.objects{o}.name);
                    
           object = frames{f}.objects{o};
           obj_pos = [object.position.y; -object.position.z; object.position.x]*10; % UE to CV
           obj_rot = [object.rotation.p; object.rotation.y; object.rotation.r];
           
           obj_bbox = objects_bbox( object.name );
           
           % call method
           obj_bbox_2d = world2camera_bbox_2dproj( obj_bbox, camera_pos, camera_rot, obj_pos, obj_rot, camera_K );
           
           % plot 2d-3d bbox
           out_of_camera_vp = sum(any(obj_bbox_2d<0));
           out_of_camera_vp = out_of_camera_vp + any(obj_bbox_2d(1,:)> camera_K.width);
           out_of_camera_vp = out_of_camera_vp + any(obj_bbox_2d(2,:)> camera_K.height);
           
           % object rgb mask
           object_label = objects_rgb_mask(frames{f}.objects{o}.name);
           obj_mask = mask_img(:,:,1)==object_label(1) & mask_img(:,:,2)==object_label(2) & mask_img(:,:,3)==object_label(3);
           obj_mask = obj_mask * 255;
           obj_mask_img = cat(3, obj_mask, obj_mask, obj_mask);
           
           % if obj inside viewpoint and any pixel is visible...
           if(out_of_camera_vp==0 && any(obj_mask(:) > 0.5))
               output_mask_frame_path = sprintf('%s%s\\mask\\%s_%06d.png',out_path,frames{f}.objects{o}.name,cameras{c}.name,frames{f}.id_generated);
               imwrite(obj_mask_img, output_mask_frame_path);

               output_rgb_frame_path = sprintf('%s%s\\JPEGImages\\%s_%06d.jpg',out_path,frames{f}.objects{o}.name,cameras{c}.name,frames{f}.id_generated);
               copyfile( rgb_frame_path, output_rgb_frame_path );

               draw_3d_projected_bbox_2d(obj_bbox_2d);

               output_txt_label_path = sprintf('%s%s\\labels\\%s_%06d.txt',out_path,frames{f}.objects{o}.name,cameras{c}.name,frames{f}.id_generated);
               fid = fopen( output_txt_label_path, 'wt' );
               % class label
               fprintf( fid, '0 ');
               % 9 points - 2d coordinates
               % same order than btekin code
               fprintf( fid, '%f %f ', (obj_bbox_2d(1,1) / camera_K.width), (obj_bbox_2d(2,1) / camera_K.height) ); % centroid

               fprintf( fid, '%f %f ', (obj_bbox_2d(1,5) / camera_K.width), (obj_bbox_2d(2,5) / camera_K.height) );
               fprintf( fid, '%f %f ', (obj_bbox_2d(1,4) / camera_K.width), (obj_bbox_2d(2,4) / camera_K.height) );
               fprintf( fid, '%f %f ', (obj_bbox_2d(1,2) / camera_K.width), (obj_bbox_2d(2,2) / camera_K.height) );
               fprintf( fid, '%f %f ', (obj_bbox_2d(1,3) / camera_K.width), (obj_bbox_2d(2,3) / camera_K.height) );

               fprintf( fid, '%f %f ', (obj_bbox_2d(1,9) / camera_K.width), (obj_bbox_2d(2,9) / camera_K.height) );
               fprintf( fid, '%f %f ', (obj_bbox_2d(1,8) / camera_K.width), (obj_bbox_2d(2,8) / camera_K.height) );
               fprintf( fid, '%f %f ', (obj_bbox_2d(1,6) / camera_K.width), (obj_bbox_2d(2,6) / camera_K.height) );
               fprintf( fid, '%f %f ', (obj_bbox_2d(1,7) / camera_K.width), (obj_bbox_2d(2,7) / camera_K.height) );

               % 
               % x/y range
               % compute 2d box size
               min_x = min(obj_bbox_2d(1,:));
               max_x = max(obj_bbox_2d(1,:));
               min_y = min(obj_bbox_2d(2,:));
               max_y = max(obj_bbox_2d(2,:));
               fprintf( fid, '%f %f ', (max_x-min_x)/camera_K.width,(max_y-min_y)/camera_K.height );
               fclose(fid);
           end

           hold on ;
       end
       
       % save debug frame, rgb and bboxes
       output_debug_img_path = sprintf('%s\\Debug\\%s_%06d.jpg',out_path,cameras{c}.name,frames{f}.id_generated);
       h = getframe;
       im = h.cdata;
       imwrite(im,output_debug_img_path);
    end
end