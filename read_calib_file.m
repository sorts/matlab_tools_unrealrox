function [cam_calib] = read_calib_file(calib_file,start_line)

    fid = fopen(calib_file);
    line_str = textscan(fid, '%s', 1, 'delimiter', '\n', 'headerlines', start_line-1);
    intrisic_params = textscan(line_str{1}{1}, '%f');
    fclose(fid);

    fid = fopen(calib_file);
    start_line = start_line+1; 
    line_str = textscan(fid, '%s', 1, 'delimiter', '\n', 'headerlines', start_line-1);
    extrinsic_params = textscan(line_str{1}{1}, '%f');
    fclose(fid);

    [cam_calib] = get_params(intrisic_params{1}, extrinsic_params{1});





end

function [cam] = get_params(intrinsic, extrinsic)
    cam.fx = intrinsic(1);
    cam.fy = intrinsic(2);
    cam.s = intrinsic(3);
    cam.cx = intrinsic(4);
    cam.cy = intrinsic(5);
    cam.kappa = intrinsic(6:end);
    cam.rx = extrinsic(1);
    cam.ry = extrinsic(2);
    cam.rz = extrinsic(3);
    cam.x = extrinsic(4);
    cam.y = extrinsic(5);
    cam.z = extrinsic(6);
    cam.K = [cam.fx 0 cam.cx; 0 cam.fy cam.cy; 0 0 1];

    cam.R = rodrigues(extrinsic(1:3));
    cam.t = [cam.x; cam.y; cam.z];
end

