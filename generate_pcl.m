function [points_3D RGB_texture]=generate_pcl(cx,cy,fx,fy,aligned_depth, aligned_rgb, R,t)

    if(~exist('R','var'))
        R=eye(3,3);
        t=[0 0 0]';
    end

    [points_3D valid_pts]= depth_to_pts(cx,cy, fx, fy,aligned_depth, size(aligned_depth,2),size(aligned_depth,1));
    
    points_3D = R * points_3D +repmat(t, [1 size(points_3D, 2)]);
    
    if(~isempty(aligned_rgb))
        RGB_texture = reshape(aligned_rgb, [], 3)';
        RGB_texture = RGB_texture(1:3, valid_pts(:));
    else
        RGB_texture=[];
    end

end