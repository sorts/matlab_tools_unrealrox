close all
clear

folders = {'Camera1', 'Camera2', 'Camera3', 'LeftHandCamera'};
cameras_pos = [ [22.412000; -100.0; -175.927993], [22.412000; -150.0; -175.927993], [22.412000; -190.0; -175.927993], [-3.3229999542236328; -117.29599761962891; 53.831001281738281] ];
cameras_rot = [ [0,0,0]; [0,0,0]; [-30,0,0]; [-88.969963073730469, 132.46774291992188,142.96835327148438] ];

out_path='results\\';
mkdir(out_path);

for k=1:length(folders)
    path_images='E:\\robotrixBBOX3D\\v2\\';

    max_depth=4000;
    height=1080;
    width=1920;

    fx = 960;
    fy = 960;
    cx = width/2;
    cy = height/2;

    folder = folders{k};
    color_img=imread(strcat(path_images,'\\rgb\\',folder,'\\000001.jpg'));
    depth_img=double(imread(strcat(path_images,'\\depth\\',folder,'\\000001.png')));

    %figure(1); imagesc(color_img);
    %figure(2); imagesc(depth_img);
    camera_pos = cameras_pos(:,k);
    camera_pos = camera_pos * 10;

    euler = cameras_rot(k,:);
    radians = degtorad(euler);
    R=eul2rot(radians);   
    
    [points_3D, RGB_texture]=generate_pcl(cx,cy,fx,fy,depth_img, color_img, R, camera_pos);

    valid=points_3D(3,:)<9000;
    points_3D(:,~valid)=[];
    RGB_texture(:,~valid)=[];  
    write_pcl([out_path folder '-pcl.ply'],points_3D,RGB_texture);
end
    
    
